/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.controller;

import org.opendicomviewer.ApplicationContext;

public class ControllerContext {
	
	private ApplicationContext applicationContext;
	
	public ControllerContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
	
	private FileController fileController;
	
	public FileController getFileController() {
		if(fileController==null) {
			fileController = new FileController(applicationContext);
		}
		return fileController;
	}

	private EditController editController;
	
	public EditController getEditController() {
		if(editController==null) {
			editController = new EditController(applicationContext);
		}
		return editController;
	}

	private HelpController helpController;
	
	public HelpController getHelpController() {
		if(helpController==null) {
			helpController = new HelpController(applicationContext);
		}
		return helpController;
	}

	private MiscellaneousController miscellaneousController;
	
	public MiscellaneousController getMiscellaneousController() {
		if(miscellaneousController==null) {
			miscellaneousController = new MiscellaneousController();
		}
		return miscellaneousController;
	}

}
