/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.controller;

import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import org.jdesktop.application.Action;
import org.opendicomviewer.ApplicationContext;
import org.opendicomviewer.util.ClipboardImage;

public class EditController {
	
	private ApplicationContext applicationContext;
	
	private ClipboardImage clipboardImage;
	
	public EditController(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
		clipboardImage = new ClipboardImage();
	}
	
	@Action
	public void edit() {
		
	}
	
	@Action
	public void copyToClipboard() {
		// TODO
		JPanel panel = applicationContext.getViewContext().getOpenDicomViewerPanel();
		BufferedImage bufferedImage = new BufferedImage(panel.getWidth(), panel.getHeight(), BufferedImage.TYPE_INT_RGB);
		panel.paint(bufferedImage.getGraphics());
		clipboardImage.setImage(bufferedImage);
	}

}
