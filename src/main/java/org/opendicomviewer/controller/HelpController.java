/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.controller;

import java.awt.Desktop;
import java.net.URI;

import javax.swing.JOptionPane;

import org.jdesktop.application.Action;
import org.opendicomviewer.ApplicationContext;

public class HelpController {
	
	private ApplicationContext applicationContext;
	
	public HelpController(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
	
	@Action
	public void help() {
		
	}

	@Action
	public void onlineHelp() {
		try {
			Desktop.getDesktop().browse(new URI("http://www.opendicomviewer.org/index.php?page=help"));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Action
	public void about() {
		applicationContext.getViewContext().getAboutDialog().setVisible(true);
	}

	@Action
	public void license() {
		try {
			Desktop.getDesktop().browse(new URI("http://www.gnu.org/licenses/lgpl-3.0-standalone.html"));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Action
	public void visit() {
		try {
			Desktop.getDesktop().browse(new URI("http://www.opendicomviewer.org"));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
		}
	}

}
