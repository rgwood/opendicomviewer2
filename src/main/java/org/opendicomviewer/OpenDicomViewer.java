/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer;

import javax.swing.ActionMap;
import javax.swing.JMenu;

import org.dcm4che2.data.UID;
import org.jdesktop.application.Application;
import org.opendicomviewer.plugin.PluginContext;
import org.opendicomviewer.plugin.image.ImagePlugin;
import org.opendicomviewer.plugin.pdf.EncapsulatedPDFPlugin;

public class OpenDicomViewer {

	private static final long serialVersionUID = 1L;
	
	private ApplicationContext applicationContext;
	
	public OpenDicomViewer() {
		applicationContext = new ApplicationContext();
	}
	
	public void startup() throws Exception {
		
		applicationContext.getViewContext().getViewerManager().addViewerManagerListener(applicationContext.getViewContext().getOpenDicomViewerPanel());
		
		ActionMap miscellaneousActionMap = Application.getInstance().getContext().getActionMap(applicationContext.getControllerContext().getMiscellaneousController());
		ActionMap fileActionMap = Application.getInstance().getContext().getActionMap(applicationContext.getControllerContext().getFileController());
		JMenu fileMenu = new JMenu(miscellaneousActionMap.get("file"));
		fileMenu.add(fileActionMap.get("openDicomDir"));
		fileMenu.add(fileActionMap.get("openDicomFiles"));
		fileMenu.add(fileActionMap.get("openDicomURL"));
		fileMenu.addSeparator();
		fileMenu.add(miscellaneousActionMap.get("exit"));
		applicationContext.getViewContext().getMenuManager().register("FILE MENU", fileMenu);
		
		ActionMap editActionMap = Application.getInstance().getContext().getActionMap(applicationContext.getControllerContext().getEditController());
		JMenu editMenu = new JMenu(editActionMap.get("edit"));
		editMenu.add(editActionMap.get("copyToClipboard"));
		applicationContext.getViewContext().getMenuManager().register("EDIT MENU", editMenu);
		
		PluginContext pluginContext = applicationContext.getPluginContext();
		
		ImagePlugin imagePlugin = new ImagePlugin();
		pluginContext.register(UID.MRImageStorage, imagePlugin);
		pluginContext.register(UID.EnhancedMRImageStorage, imagePlugin);
		pluginContext.register(UID.CTImageStorage, imagePlugin);
		pluginContext.register(UID.EnhancedCTImageStorage, imagePlugin);
		
		EncapsulatedPDFPlugin encapsulatedPDFPlugin = new EncapsulatedPDFPlugin();
		pluginContext.register(UID.EncapsulatedPDFStorage, encapsulatedPDFPlugin);
		
		ActionMap helpActionMap = Application.getInstance().getContext().getActionMap(applicationContext.getControllerContext().getHelpController());
		JMenu helpMenu = new JMenu(helpActionMap.get("help"));
		helpMenu.add(helpActionMap.get("onlineHelp"));
		helpMenu.addSeparator();
		helpMenu.add(helpActionMap.get("about"));
		applicationContext.getViewContext().getMenuManager().register("HELP MENU", helpMenu);
		
		applicationContext.getViewContext().getDicomThumbsPane().getDicomThumbsModel().clear();
		
	}
	
	public void shutdown() throws Exception {
		
		PluginContext pluginContext = applicationContext.getPluginContext();

		pluginContext.unregister(UID.MRImageStorage);
		pluginContext.unregister(UID.EnhancedMRImageStorage);
		pluginContext.unregister(UID.CTImageStorage);
		pluginContext.unregister(UID.EnhancedCTImageStorage);
		pluginContext.unregister(UID.EncapsulatedPDFStorage);
			
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	
}
