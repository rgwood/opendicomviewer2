/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer;

import org.opendicomviewer.controller.ControllerContext;
import org.opendicomviewer.model.ModelContext;
import org.opendicomviewer.plugin.PluginContext;
import org.opendicomviewer.view.ViewContext;

public class ApplicationContext {
	
	private ControllerContext controllerContext;
	
	private ModelContext modelContext;
	
	private ViewContext viewContext;
	
	private PluginContext pluginContext;

	public ApplicationContext() {
		controllerContext = new ControllerContext(this);
		modelContext = new ModelContext(this);
		viewContext = new ViewContext(this);
		pluginContext = new PluginContext(this);
	}
	
	public ControllerContext getControllerContext() {
		return controllerContext;
	}

	public ModelContext getModelContext() {
		return modelContext;
	}

	public ViewContext getViewContext() {
		return viewContext;
	}

	public PluginContext getPluginContext() {
		return pluginContext;
	}

}
