/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.view;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.text.SimpleDateFormat;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.opendicomviewer.model.Patient;

public class DicomInfoPane extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private JLabel patientNameLabel;
	private JLabel patientIdLabel;
	private JLabel patientBirthDateLabel;
	private JLabel patientSexLabel;

	public DicomInfoPane() {

		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createEmptyBorder(10, 5, 5, 5));
		
		int row = 0;
		
		patientNameLabel = createFieldLabel("");
		add(patientNameLabel,createConstraints(row++));

		add(new JLabel("Patient ID:"),createLabelConstraints(row));
		patientIdLabel = createFieldLabel("");
		add(patientIdLabel,createFieldConstraints(row++));
		
		add(new JLabel("Date of Birth:"),createLabelConstraints(row));
		patientBirthDateLabel = createFieldLabel("");
		add(patientBirthDateLabel,createFieldConstraints(row++));

		add(new JLabel("Patient Sex:"),createLabelConstraints(row));
		patientSexLabel = createFieldLabel("");
		add(patientSexLabel,createFieldConstraints(row++));

	}
	
	public void loadPatientInfo(Patient patient) {
		
		patientNameLabel.setText(patient.getPatientName());
		
		if(patient.getPatientId()!=null) {
			patientIdLabel.setText(patient.getPatientId());
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		if(patient.getPatientBirthDate()!=null) {
			patientBirthDateLabel.setText(dateFormat.format(patient.getPatientBirthDate()));
		}
		
		if(patient.getPatientSex()!=null) {
			patientSexLabel.setText(patient.getPatientSex());
		}
		
	}
	
	private JLabel createFieldLabel(String text) {
		JLabel label = new JLabel(text);
		Font font = label.getFont();
		label.setFont(new Font(font.getName(), Font.BOLD, font.getSize()));
		return label;
	}
	
	private GridBagConstraints createConstraints(int row) {
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = row;
		constraints.gridwidth = 2;
		return constraints;
		
	}

	private GridBagConstraints createLabelConstraints(int row) {
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = row;
		return constraints;
		
	}

	private GridBagConstraints createFieldConstraints(int row) {
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 1;
		constraints.gridy = row;
		return constraints;
		
	}

}
