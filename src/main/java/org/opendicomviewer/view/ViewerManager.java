/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.view;

import java.util.HashMap;
import java.util.LinkedList;

import javax.swing.JPanel;

public class ViewerManager {
	
	private HashMap<String, JPanel> viewerMap;
	private LinkedList<JPanel> viewers;
	private LinkedList<ViewerManagerListener> listeners;
	
	protected ViewerManager() {
		viewerMap = new HashMap<String, JPanel>();
		viewers = new LinkedList<JPanel>();
		listeners = new LinkedList<ViewerManagerListener>();
	}
	
	public void register(String id, JPanel viewer) {
		JPanel oldPanel = viewerMap.get(id);
		if(oldPanel!=null) {
			unregister(id);
		}
		viewerMap.put(id, viewer);
		viewers.addLast(viewer);
		fireViewerRegistered(id,viewer);
	}

	public void unregister(String id) {
		JPanel viewer = viewerMap.get(id);
		if(viewer!=null) {
			viewerMap.remove(id);
			viewers.remove(viewer);
			fireViewerUnregistered(id,viewer);
		}
	}
	
	public JPanel[] getViewers() {
		return viewers.toArray(new JPanel[viewers.size()]);
	}
	
	public JPanel getViewer(Object id) {
		return viewerMap.get(id);
	}
	
	public void fireViewerRegistered(String id, JPanel source) {
		for(ViewerManagerListener listener : listeners) {
			listener.viewerRegistered(id, source);
		}
	}
	
	public void fireViewerUnregistered(String id, JPanel source) {
		for(ViewerManagerListener listener : listeners) {
			listener.viewerUnregistered(id, source);
		}
	}

	public void addViewerManagerListener(ViewerManagerListener listener) {
		listeners.addLast(listener);
	}

	public void removeViewerManagerListener(ViewerManagerListener listener) {
		listeners.remove(listener);
	}

}
