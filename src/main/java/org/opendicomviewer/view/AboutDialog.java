/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

import org.jdesktop.application.Application;
import org.opendicomviewer.ApplicationContext;
import org.opendicomviewer.controller.HelpController;

public class AboutDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	
	private ActionMap actionMap;

	public AboutDialog(ApplicationContext applicationContext) {
		super(applicationContext.getViewContext().getOpenDicomViewerFrame(), true);
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(AboutDialog.class.getResource("/org/opendicomviewer/view/resources/OpenDicomViewer.png")));
		setTitle("Open Dicom Viewer");
		
		setResizable(false);
		setSize(500, 200);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension dimension = toolkit.getScreenSize();
		setLocation((int) (dimension.getWidth()-425)/2,(int)  (dimension.getHeight()-200)/2);
		
		HelpController helpController = applicationContext.getControllerContext().getHelpController();
		actionMap = Application.getInstance().getContext().getActionMap(helpController);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JLabel label = new JLabel();
		label.setIcon(new ImageIcon(AboutDialog.class.getResource("/org/opendicomviewer/view/resources/OpenDicomViewer_HD.png")));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.EAST;
		gbc_label.gridheight = 5;
		gbc_label.insets = new Insets(0, 0, 0, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		getContentPane().add(label, gbc_label);
		
		JLabel lblOpenDicomViewer = new JLabel("Open Dicom Viewer");
		lblOpenDicomViewer.setFont(new Font("Dialog", Font.BOLD, 18));
		GridBagConstraints gbc_lblOpenDicomViewer = new GridBagConstraints();
		gbc_lblOpenDicomViewer.gridwidth = 2;
		gbc_lblOpenDicomViewer.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblOpenDicomViewer.insets = new Insets(0, 0, 5, 0);
		gbc_lblOpenDicomViewer.gridx = 1;
		gbc_lblOpenDicomViewer.gridy = 0;
		getContentPane().add(lblOpenDicomViewer, gbc_lblOpenDicomViewer);
		
		JLabel lblVersionsnapshot = new JLabel("Version:");
		lblVersionsnapshot.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_lblVersionsnapshot = new GridBagConstraints();
		gbc_lblVersionsnapshot.insets = new Insets(0, 0, 5, 5);
		gbc_lblVersionsnapshot.anchor = GridBagConstraints.WEST;
		gbc_lblVersionsnapshot.gridx = 1;
		gbc_lblVersionsnapshot.gridy = 1;
		getContentPane().add(lblVersionsnapshot, gbc_lblVersionsnapshot);
		
		JLabel label_1 = new JLabel("0.9.0 BETA");
		label_1.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.WEST;
		gbc_label_1.insets = new Insets(0, 0, 5, 0);
		gbc_label_1.gridx = 2;
		gbc_label_1.gridy = 1;
		getContentPane().add(label_1, gbc_label_1);
		
		JLabel lblLicenseGplv = new JLabel("License:");
		lblLicenseGplv.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_lblLicenseGplv = new GridBagConstraints();
		gbc_lblLicenseGplv.anchor = GridBagConstraints.WEST;
		gbc_lblLicenseGplv.insets = new Insets(0, 0, 5, 5);
		gbc_lblLicenseGplv.gridx = 1;
		gbc_lblLicenseGplv.gridy = 2;
		getContentPane().add(lblLicenseGplv, gbc_lblLicenseGplv);
		
		JLabel lblGnuGeneralPublic = new JLabel("GNU Lesser General Public License");
		lblGnuGeneralPublic.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Action action = actionMap.get("license");
				ActionEvent ae = new ActionEvent(e.getSource(), ActionEvent.ACTION_PERFORMED, "license");
				action.actionPerformed(ae);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				setCursor(Cursor.getDefaultCursor());
			}
		});
		lblGnuGeneralPublic.setForeground(Color.BLUE);
		lblGnuGeneralPublic.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_lblGnuGeneralPublic = new GridBagConstraints();
		gbc_lblGnuGeneralPublic.anchor = GridBagConstraints.WEST;
		gbc_lblGnuGeneralPublic.insets = new Insets(0, 0, 5, 0);
		gbc_lblGnuGeneralPublic.gridx = 2;
		gbc_lblGnuGeneralPublic.gridy = 2;
		getContentPane().add(lblGnuGeneralPublic, gbc_lblGnuGeneralPublic);
		
		JLabel lblVisit = new JLabel("Visit:");
		lblVisit.setFont(new Font("Dialog", Font.PLAIN, 12));
		GridBagConstraints gbc_lblVisit = new GridBagConstraints();
		gbc_lblVisit.anchor = GridBagConstraints.WEST;
		gbc_lblVisit.insets = new Insets(0, 0, 5, 5);
		gbc_lblVisit.gridx = 1;
		gbc_lblVisit.gridy = 3;
		getContentPane().add(lblVisit, gbc_lblVisit);
		
		JLabel lblWwwopendicomviewerorg = new JLabel("www.opendicomviewer.org");
		lblWwwopendicomviewerorg.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblWwwopendicomviewerorg.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Action action = actionMap.get("visit");
				ActionEvent ae = new ActionEvent(e.getSource(), ActionEvent.ACTION_PERFORMED, "visit");
				action.actionPerformed(ae);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				setCursor(Cursor.getDefaultCursor());
			}
		});
		lblWwwopendicomviewerorg.setForeground(Color.BLUE);
		GridBagConstraints gbc_lblWwwopendicomviewerorg = new GridBagConstraints();
		gbc_lblWwwopendicomviewerorg.insets = new Insets(0, 0, 5, 0);
		gbc_lblWwwopendicomviewerorg.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblWwwopendicomviewerorg.gridx = 2;
		gbc_lblWwwopendicomviewerorg.gridy = 3;
		getContentPane().add(lblWwwopendicomviewerorg, gbc_lblWwwopendicomviewerorg);
		
	}

}
