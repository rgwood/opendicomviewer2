/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.view;

import java.util.HashMap;
import java.util.LinkedList;

import javax.swing.JMenu;

public class MenuManager {
	
	private HashMap<String, JMenu> menuMap;
	private LinkedList<JMenu> menus;
	private LinkedList<MenuManagerListener> listeners;
	
	protected MenuManager() {
		menuMap = new HashMap<String, JMenu>();
		menus = new LinkedList<JMenu>();
		listeners = new LinkedList<MenuManagerListener>();
	}
	
	public void register(String id, JMenu menu) {
		JMenu oldMenu = menuMap.get(id);
		if(oldMenu!=null) {
			unregister(id);
		}
		menuMap.put(id, menu);
		menus.addLast(menu);
		fireMenuRegistered(id,menu);
	}

	public void unregister(String id) {
		JMenu menu = menuMap.get(id);
		if(menu!=null) {
			menuMap.remove(id);
			menus.remove(menu);
			fireMenuUnregistered(id,menu);
		}
	}
	
	public JMenu[] getMenus() {
		return menus.toArray(new JMenu[menus.size()]);
	}
	
	public JMenu getMenu(String id) {
		return menuMap.get(id);
	}
	
	public void fireMenuRegistered(String id, JMenu source) {
		for(MenuManagerListener listener : listeners) {
			listener.menuRegistered(id,source);
		}
	}
	
	public void fireMenuUnregistered(String id, JMenu source) {
		for(MenuManagerListener listener : listeners) {
			listener.menuUnregistered(id,source);
		}
	}

	public void addMenuManagerListener(MenuManagerListener listener) {
		listeners.addLast(listener);
	}

	public void removeMenuManagerListener(MenuManagerListener listener) {
		listeners.remove(listener);
	}

}
