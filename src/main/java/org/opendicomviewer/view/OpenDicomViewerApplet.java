/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.view;

import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JApplet;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.jdesktop.application.Application;
import org.opendicomviewer.FakeApplication;
import org.opendicomviewer.OpenDicomViewer;

public class OpenDicomViewerApplet extends JApplet implements MenuManagerListener {

	private static final long serialVersionUID = 1L;
	
	private OpenDicomViewer openDicomViewer;
	
	public OpenDicomViewerApplet() {
		
		openDicomViewer = new OpenDicomViewer();

		openDicomViewer.getApplicationContext().getViewContext().getMenuManager().addMenuManagerListener(this);

		Application.launch(FakeApplication.class, new String[0]);
		
		SwingUtilities.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					openDicomViewer.startup();
					setContentPane(openDicomViewer.getApplicationContext().getViewContext().getOpenDicomViewerPanel());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});

	}
	
	@Override
	public void init() {
		
		SwingUtilities.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					String dicomUrls = getParameter("dicom");
					if(dicomUrls!=null) {
						String[] uriSplit = dicomUrls.split(";");
						URI[] uris = new URI[uriSplit.length];
						for(int i=0;i<uriSplit.length;i++) {
							uris[i] = new URI(uriSplit[i]);
						}
						openDicomViewer.getApplicationContext().getControllerContext().getFileController().openDicomURIs(uris);
					}
				} catch (URISyntaxException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
								
			}
		});
		
	}
	
	@Override
	public void start() {

	}
	
	@Override
	public void stop() {
	}

	@Override
	public void destroy() {
		try {
			openDicomViewer.shutdown();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void menuRegistered(String id, JMenu source) {
		updateMenuBar();
	}

	public void menuUnregistered(String id, JMenu source) {
		updateMenuBar();
	}
	
	private void updateMenuBar() {
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		for(JMenu menu : openDicomViewer.getApplicationContext().getViewContext().getMenuManager().getMenus()) {
			menuBar.add(menu);
		}
		
	}

}
