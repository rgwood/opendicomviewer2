/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.view;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;
import org.opendicomviewer.ApplicationContext;

public class OpenDicomViewerFrame extends JFrame implements MenuManagerListener {
	
	private static final long serialVersionUID = 1L;
	
	private ApplicationContext applicationContext;
	
	public OpenDicomViewerFrame(ApplicationContext applicationContext) {
		
		this.applicationContext = applicationContext;
		
		ResourceMap resourceMap = Application.getInstance().getContext().getResourceMap(OpenDicomViewerFrame.class);
		
		setTitle(resourceMap.getString("application.title"));
		setIconImage(resourceMap.getImageIcon("application.icon").getImage());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
				
		setContentPane(applicationContext.getViewContext().getOpenDicomViewerPanel());
		
	}

	public void menuRegistered(String id, JMenu source) {
		updateMenuBar();
	}

	public void menuUnregistered(String id, JMenu source) {
		updateMenuBar();
	}
	
	private void updateMenuBar() {
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		for(JMenu menu : applicationContext.getViewContext().getMenuManager().getMenus()) {
			menuBar.add(menu);
		}
		
	}

}
