/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.LinkedList;

import javax.swing.AbstractListModel;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.application.Application;
import org.opendicomviewer.ApplicationContext;
import org.opendicomviewer.model.Instance;
import org.opendicomviewer.plugin.Plugin;
import org.opendicomviewer.plugin.PluginException;

public class DicomThumbsPane extends JList {

	private static final long serialVersionUID = 1L;
	
	public static class DicomThumbsModel extends AbstractListModel {

		private static final long serialVersionUID = 1L;
		
		private LinkedList<Instance> instances;
		
		public DicomThumbsModel() {
			instances = new LinkedList<Instance>();
		}

		public Object getElementAt(int index) {
			if(index<0) {
				return null;
			} else {
				return instances.get(index);
			}
		}

		public int getSize() {
			return instances.size();
		}
		
		public void addInstance(Instance instance) {
			instances.addLast(instance);
			fireIntervalAdded(this, instances.size()-1, instances.size()-1);
		}
		
		public void clear() {
			int size = instances.size();
			if(size>0) {
				instances.clear();
				fireIntervalRemoved(this, 0, size-1);
			}
		}

	}
	
	private DicomThumbsModel dicomThumbsModel;
	
	private class ImageListCellRender implements ListCellRenderer {

		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			
			Instance instance = (Instance) value;
		
			JLabel label = new JLabel();
			Dimension dim = new Dimension(120,143);
			label.setPreferredSize(dim);
			label.setMaximumSize(dim);
			
			try {
				Plugin plugin = applicationContext.getPluginContext().getPlugin(instance.getSopClassUID());
				ImageIcon icon = new ImageIcon(plugin.getThumbnail(instance));
				label.setIcon(icon);
				label.setOpaque(true);
				label.setText(instance.getSeries().getModality()+" Series: "+instance.getSeries().getSeriesNumber());
				label.setVerticalTextPosition(SwingConstants.BOTTOM);
				label.setHorizontalTextPosition(SwingConstants.CENTER);
			} catch (PluginException e) {
				e.printStackTrace();
			}

			if(isSelected){
				label.setBackground(Color.BLUE);
				label.setBorder(BorderFactory.createLineBorder(Color.BLUE));
				label.setForeground(Color.WHITE);
			}
			else{
				label.setBackground(Color.DARK_GRAY);
				label.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
				label.setForeground(Color.LIGHT_GRAY);
			}
			
			return label;
		
		}
	}
	
	private ApplicationContext applicationContext;
	
	public DicomThumbsPane(ApplicationContext applicationContext) {
		
		this.applicationContext = applicationContext;
		
		dicomThumbsModel = new DicomThumbsModel();
		setModel(dicomThumbsModel);
		
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setBackground(Color.BLACK);
		setCellRenderer(new ImageListCellRender());
		setLayoutOrientation(JList.HORIZONTAL_WRAP);
		setVisibleRowCount(-1);
		
		getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting()==false){
					ActionMap actionMap = Application.getInstance().getContext().getActionMap(DicomThumbsPane.this.applicationContext.getControllerContext().getFileController());
					ActionEvent ae = new ActionEvent(e.getSource(), ActionEvent.ACTION_PERFORMED, "loadInstance");
					actionMap.get("loadInstance").actionPerformed(ae);
				}
			}
		});
		
	}

	public Instance getSelectedInstance() {
		DicomThumbsModel dicomThumbsModel = (DicomThumbsModel) getModel();
		Instance selected = (Instance) dicomThumbsModel.getElementAt(getSelectionModel().getMinSelectionIndex());
		return selected;
	}

	public DicomThumbsModel getDicomThumbsModel() {
		return dicomThumbsModel;
	}
	
}
