/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.Timer;

import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.TaskMonitor;

public class StatusBarPane extends JPanel {

	private static final long serialVersionUID = 1L;
	
    private JProgressBar progressBar;
    private JLabel statusAnimationLabel;
    private JLabel statusMessageLabel;

    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    
    private TaskMonitor taskMonitor;
	
	public StatusBarPane() {
		
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		
		statusMessageLabel = new JLabel();
		add(statusMessageLabel, BorderLayout.CENTER);
		
		Box box = Box.createHorizontalBox();
		add(box, BorderLayout.EAST);

		box.add(Box.createHorizontalGlue());
		
		progressBar = new JProgressBar();
		box.add(progressBar);
		
		box.add(Box.createHorizontalStrut(5));
		
		statusAnimationLabel = new JLabel();
		box.add(statusAnimationLabel);

		ResourceMap resourceMap = Application.getInstance().getContext().getResourceMap(StatusBarPane.class);
        int busyAnimationRate = resourceMap.getInteger("busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setEnabled(false);

        taskMonitor = new TaskMonitor(Application.getInstance().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
        	
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
            	
            	synchronized (StatusBarPane.this) {
                    String propertyName = evt.getPropertyName();
                    if ("started".equals(propertyName)) {
                        updateMessage();
                    	if(!busyIconTimer.isRunning()) {
                            statusAnimationLabel.setIcon(busyIcons[0]);
                            busyIconIndex = 0;
                            busyIconTimer.start();
                    	}
                        if(!progressBar.isEnabled()) {
    	                    progressBar.setEnabled(true);
    	                    progressBar.setIndeterminate(true);
                        } else {
                        	updateProgressBar();
                        }
                    } else if ("done".equals(propertyName)) {
                        updateMessage();
                    	if(taskMonitor.getTasks().size()==0) {
                            busyIconTimer.stop();
                            statusAnimationLabel.setIcon(idleIcon);
                            progressBar.setEnabled(false);
                            progressBar.setIndeterminate(false);
                            progressBar.setValue(0);
                    	} else {
                    		updateProgressBar();
                    	}
                    } else if ("message".equals(propertyName)) {
                        updateMessage();
                        updateProgressBar();
                    } else if ("progress".equals(propertyName)) {
                        updateMessage();
                        updateProgressBar();
                    }
				}

            }
            
            private void updateMessage() {
            	StringBuffer message = new StringBuffer();
            	if(taskMonitor.getTasks().size()>0) {
            		String text = taskMonitor.getForegroundTask().getMessage();
                	if(text!=null&&!text.equals("")) {
                		message.append(text+" ");
                	}
            		if(taskMonitor.getTasks().size()>1) {
            			message.append("("+taskMonitor.getTasks().size()+" tareas en ejecución)");
            		} else {
            			message.append("(1 tarea en ejecución)");
            		}
            	}
            	statusMessageLabel.setText(message.toString());
            }
            
            private void updateProgressBar() {
                int value = taskMonitor.getForegroundTask().getProgress();
                progressBar.setVisible(true);
                progressBar.setIndeterminate(false);
                progressBar.setValue(value);
            }
            
        });
        
	}

}
