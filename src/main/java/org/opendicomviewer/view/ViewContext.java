/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.view;

import org.opendicomviewer.ApplicationContext;

public class ViewContext {
	
	private ApplicationContext applicationContext;
	
	public ViewContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	private OpenDicomViewerFrame openDicomViewerFrame;
	
	public OpenDicomViewerFrame getOpenDicomViewerFrame() {
		if(openDicomViewerFrame==null) {
			openDicomViewerFrame = new OpenDicomViewerFrame(applicationContext);
		}
		return openDicomViewerFrame;
	}

	private OpenDicomViewerPanel openDicomViewerPanel;
	
	public OpenDicomViewerPanel getOpenDicomViewerPanel() {
		if(openDicomViewerPanel==null) {
			openDicomViewerPanel = new OpenDicomViewerPanel(applicationContext);
		}
		return openDicomViewerPanel;
	}

	private DicomThumbsPane dicomThumbsPane;
	
	public DicomThumbsPane getDicomThumbsPane() {
		if(dicomThumbsPane==null) {
			dicomThumbsPane = new DicomThumbsPane(applicationContext);
		}
		return dicomThumbsPane;
	}

	private DicomInfoPane dicomInfoPane;
	
	public DicomInfoPane getDicomInfoPane() {
		if(dicomInfoPane==null) {
			dicomInfoPane = new DicomInfoPane();
		}
		return dicomInfoPane;
	}

	private AboutDialog aboutDialog;
	
	public AboutDialog getAboutDialog() {
		if(aboutDialog==null) {
			aboutDialog = new AboutDialog(applicationContext);
		}
		return aboutDialog;
	}
	
	private StatusBarPane statusBarPane;
	
	public StatusBarPane getStatusBarPane() {
		if(statusBarPane==null) {
			statusBarPane = new StatusBarPane();
		}
		return statusBarPane;
	}

	private MenuManager menuManager;
	
	public MenuManager getMenuManager() {
		if(menuManager==null) {
			menuManager = new MenuManager();
		}
		return menuManager;
	}

	private ViewerManager ViewerManager;
	
	public ViewerManager getViewerManager() {
		if(ViewerManager==null) {
			ViewerManager = new ViewerManager();
		}
		return ViewerManager;
	}

}
