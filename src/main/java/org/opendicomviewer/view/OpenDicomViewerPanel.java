/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;
import org.opendicomviewer.ApplicationContext;

public class OpenDicomViewerPanel extends JPanel implements ViewerManagerListener {
	
	private static final long serialVersionUID = 1L;
	
	private ApplicationContext applicationContext;
	
	private JPanel centerPanel;
	private CardLayout centerCardLayout;
	
	private String currentPluginId;

	public OpenDicomViewerPanel(ApplicationContext applicationContext) {
		
		this.applicationContext = applicationContext;

		ResourceMap resourceMap = Application.getInstance().getContext().getResourceMap(OpenDicomViewerPanel.class);
		
		setLayout(new BorderLayout());
		
		JPanel westPanel = new JPanel();
		westPanel.setLayout(new BorderLayout());
		JScrollPane thumbsScroll = new JScrollPane(applicationContext.getViewContext().getDicomThumbsPane());
		thumbsScroll.setBorder(null);
		thumbsScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		thumbsScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		thumbsScroll.setPreferredSize(new Dimension(263+thumbsScroll.getWidth(),250));
		westPanel.add(thumbsScroll, BorderLayout.CENTER);
		westPanel.add(applicationContext.getViewContext().getDicomInfoPane(), BorderLayout.NORTH);
		add(westPanel, BorderLayout.WEST);
		
		centerPanel = new JPanel();
		centerCardLayout = new CardLayout();
		centerPanel.setLayout(centerCardLayout);
		add(centerPanel, BorderLayout.CENTER);
		
		add(applicationContext.getViewContext().getStatusBarPane(), BorderLayout.SOUTH);
		
	}

	public void showViewer(String id) {
		
		if(currentPluginId!=null) {
			JMenu currentMenu = applicationContext.getViewContext().getMenuManager().getMenu(currentPluginId);
			if(currentMenu!=null) {
				currentMenu.setVisible(false);
			}
		}
		
		JMenu newMenu = applicationContext.getViewContext().getMenuManager().getMenu(id);
		if(newMenu!=null) {
			newMenu.setVisible(true);
		}

		centerCardLayout.show(centerPanel, id);
		
		currentPluginId = id;
		
	}

	public void viewerRegistered(String id, JPanel source) {
		centerPanel.add(source,id);
	}

	public void viewerUnregistered(String id, JPanel source) {
		centerPanel.remove(source);
	}

}
