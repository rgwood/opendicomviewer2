package org.opendicomviewer.plugin.image;

public interface ImageBufferListener {
	
	public void imageLoaded(int index);

}
