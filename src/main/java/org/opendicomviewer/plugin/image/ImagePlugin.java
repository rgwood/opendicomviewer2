/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.plugin.image;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Iterator;

import javax.activation.DataSource;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ActionMap;
import javax.swing.JMenu;
import javax.swing.JPanel;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.imageio.plugins.dcm.DicomImageReadParam;
import org.dcm4che2.imageioimpl.plugins.dcm.DicomImageReader;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.StopTagInputHandler;
import org.dcm4che2.util.CloseUtils;
import org.jdesktop.application.Application;
import org.opendicomviewer.ApplicationContext;
import org.opendicomviewer.controller.FileController;
import org.opendicomviewer.model.Instance;
import org.opendicomviewer.plugin.Plugin;
import org.opendicomviewer.plugin.PluginException;
import org.opendicomviewer.view.MenuManager;
import org.opendicomviewer.view.ViewerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImagePlugin implements Plugin {
	
	private ImageViewer imageViewer;
	private ImageController imageController;
	private ImageToolBar imageToolBar;
	private JPanel view;
	private JMenu menu;
	
	private boolean installed;
	
	public ImagePlugin() {
		
		imageViewer = new ImageViewer();
		imageController = new ImageController(imageViewer);
		imageToolBar = new ImageToolBar(imageController,imageViewer);
		
		view = new JPanel();
		view.setLayout(new BorderLayout());
		view.add(imageToolBar,BorderLayout.WEST);
		view.add(imageViewer,BorderLayout.CENTER);
		
		menu = createMenu();
		
	}

	public void install(String cuid, ApplicationContext applicationContext) {
		
		if(!installed) {
			applicationContext.getViewContext().getViewerManager().register(getId(),view);
			applicationContext.getViewContext().getMenuManager().register(getId(), menu);
			installed = true;
		}
		
	}

	public void uninstall(String cuid, ApplicationContext applicationContext) {
		
		if(installed) {
			applicationContext.getViewContext().getViewerManager().unregister(getId());
			applicationContext.getViewContext().getMenuManager().unregister(getId());
			installed = false;
		}
		
	}

	public void viewInstance(Instance instance) throws PluginException {
		try {
			imageViewer.loadImage((org.opendicomviewer.plugin.image.Image) instance);
		} catch (IOException e) {
			e.printStackTrace();
			throw new PluginException(e.getMessage(),e);
		}
	}

	public java.awt.Image getThumbnail(Instance instance) {
		Image image = (Image) instance;
		return image.getThumbnail();
	}

	public String getId() {
		return "IMAGE PLUGIN";
	}

	private JMenu createMenu() {
		
		ActionMap imageActionMap = Application.getInstance().getContext().getActionMap(imageController);
		
		menu = new JMenu(imageActionMap.get("image"));
		menu.setVisible(false);
		menu.add(imageActionMap.get("previousImage"));
		menu.add(imageActionMap.get("nextImage"));
		menu.add(imageActionMap.get("playStop"));
		menu.addSeparator();
		menu.add(imageActionMap.get("selectScroll"));
		menu.add(imageActionMap.get("selectZoom"));
		menu.add(imageActionMap.get("selectMove"));
		menu.add(imageActionMap.get("selectContrast"));
		menu.addSeparator();
		menu.add(imageActionMap.get("imageInformation"));
			
		return menu;

	}

	public Instance createInstance(DataSource dataSource) throws PluginException {
		
		try {
			
			DicomObject dcmObj = null;
			DicomInputStream din = new DicomInputStream(dataSource.getInputStream());
			din.setHandler(new StopTagInputHandler(Tag.PixelData - 1));
			dcmObj = new BasicDicomObject();
			din.readDicomObject(dcmObj,-1);
			din.close();

			Image image = new Image();
			image.setDataSource(dataSource);
			image.setThumbnail(loadThumbnail(dataSource));
			image.setSopClassUID(dcmObj.getString(Tag.SOPClassUID));
			image.setSopInstanceUID(dcmObj.getString(Tag.SOPInstanceUID));
			image.setAcquisitionDateTime(dcmObj.getDate(Tag.AcquisitionDateTime));
			image.setInstanceNumber(dcmObj.getString(Tag.InstanceNumber));
			image.setNumberOfFrames(dcmObj.getInt(Tag.NumberOfFrames));
			image.setRows(dcmObj.getInt(Tag.Rows));
			image.setColumns(dcmObj.getInt(Tag.Columns));
			l.debug("dcm={}", dcmObj);
			return image;
		} catch (IOException ex) {
			l.debug("ex="+ex, ex);
			//ex.printStackTrace();
			throw new PluginException(ex.getMessage(), ex);
		}

	}
	private static final Logger l= LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private java.awt.Image loadThumbnail(DataSource dataSource) throws IOException {
		
        ImageInputStream iis = null;
        DicomImageReader reader = null;
        try {
            Iterator<ImageReader> it = ImageIO.getImageReadersByFormatName("DICOM");
            reader = (DicomImageReader) it.next();
            iis = ImageIO.createImageInputStream(new BufferedInputStream(dataSource.getInputStream(),4096));
            reader.setInput(iis,true,true);
            DicomImageReadParam param = (DicomImageReadParam) reader.getDefaultReadParam();
            param.setWindowCenter(0.0f);
            param.setWindowWidth(0.0f);
            param.setVoiLutFunction(null);
            param.setPresentationState(null);
            param.setPValue2Gray(null);
            param.setAutoWindowing(false);
            BufferedImage img = reader.read(0, param);
           	BufferedImage bi = new BufferedImage(120, 120, BufferedImage.TYPE_INT_RGB);
           	Graphics g = bi.getGraphics();
           	g.drawImage(img, 0, 0, 119, 119, Color.BLACK, null);
           	return bi;
        } finally {
        	if(reader!=null){
        		reader.dispose();
        	}
        	if(iis!=null){
        		CloseUtils.safeClose(iis);
        	}
        }
        
	}
	
}
