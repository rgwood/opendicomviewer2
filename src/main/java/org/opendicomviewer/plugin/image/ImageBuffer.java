/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.plugin.image;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.dcm4che2.imageio.plugins.dcm.DicomImageReadParam;
import org.dcm4che2.imageioimpl.plugins.dcm.DicomImageReader;
import org.dcm4che2.util.CloseUtils;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;

public class ImageBuffer {
	
	private Image image;
	
	private BufferedImage defaultBuffer;
	
	private int numberOfFrames;
	
	private BufferedImage[] buffers;
	
	private ImageLoader imageLoader;
	
	private ArrayList<ImageBufferListener> listeners;
	
	public ImageBuffer() {
		buffers = new BufferedImage[0];
		listeners = new ArrayList<ImageBufferListener>();
	}

	public synchronized void setImage(Image image) {
		this.image = image;
		loadFrames();
	}
	
	private void loadFrames() {
		
		if(imageLoader!=null) {
			imageLoader.cancel(true);
		}
		
		numberOfFrames = image.getNumberOfFrames()>0?image.getNumberOfFrames():1;
		buffers = new BufferedImage[numberOfFrames];

		defaultBuffer = new BufferedImage(image.getColumns(), image.getRows(), BufferedImage.TYPE_INT_RGB);
		for(int i=0;i<numberOfFrames;i++) {
			buffers[i] = defaultBuffer;
		}
		
		imageLoader = new ImageLoader(Application.getInstance());		
		Application.getInstance().getContext().getTaskService().execute(imageLoader);
		
	}

	private class ImageLoader extends Task<BufferedImage, Void> {
		
		public ImageLoader(Application application) {
			super(application);
		}

		@Override
		protected BufferedImage doInBackground() throws Exception {
			
			setMessage("Cargando la serie "+image.getSeries().getSeriesDescription());

			for(int i=0;i<numberOfFrames;i++) {
				
				if(isCancelled()) {
					return null;
				}
			
				setMessage("Cargando la serie "+image.getSeries().getSeriesDescription());
				setProgress(i * 100 / numberOfFrames);

				ImageInputStream iis = null;
		        DicomImageReader reader = null;
		        BufferedImage bi = null;
		        try {
		            Iterator<ImageReader> it = ImageIO.getImageReadersByFormatName("DICOM");
		            reader = (DicomImageReader) it.next();
		            iis = ImageIO.createImageInputStream(new BufferedInputStream(image.getDataSource().getInputStream()));
		            reader.setInput(iis,true,true);
		            DicomImageReadParam param = (DicomImageReadParam) reader.getDefaultReadParam();
		            param.setWindowCenter(0.0f);
		            param.setWindowWidth(0.0f);
		            param.setVoiLutFunction(null);
		            param.setPresentationState(null);
		            param.setPValue2Gray(null);
		            param.setAutoWindowing(false);
	            	bi = reader.read(i, param);
		        } finally {
		        	if(reader!=null){
		        		reader.dispose();
		        	}
		        	if(iis!=null){
		        		CloseUtils.safeClose(iis);
		        	}
		        }
		        
	        	synchronized (ImageBuffer.this) {
	        		if(!isCancelled()) {
	        			buffers[i] = bi;
	        			fireImageLoaded(i);
	        		}
				}
	        	
			}

	        return null;

		}
		
	}
	
	public synchronized int getSize() {
		return buffers.length;
	}

	public synchronized BufferedImage getBuffer(int index) {
		BufferedImage bi = buffers[index];
		if(bi==null) {
			bi = defaultBuffer;
		}
		return bi;
	}
	
	public void addImageBufferListener(ImageBufferListener listener) {
		listeners.add(listener);
	}

	public void removeImageBufferListener(ImageBufferListener listener) {
		listeners.remove(listener);
	}
	
	public void fireImageLoaded(int index) {
		for(ImageBufferListener listener : listeners) {
			listener.imageLoaded(index);
		}
	}

}
