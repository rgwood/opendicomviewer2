package org.opendicomviewer.plugin.image;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import org.opendicomviewer.model.Patient;
import org.opendicomviewer.model.Series;
import org.opendicomviewer.model.Study;

public class ImageViewer extends JPanel implements ComponentListener, MouseMotionListener, MouseWheelListener, ImageBufferListener {

	private static final long serialVersionUID = 1L;
	
	public static final int TOOLS_NONE = 0;
	public static final int TOOLS_SCROLL = 1;
	public static final int TOOLS_ZOOM = 2;
	public static final int TOOLS_MOVE = 3;
	public static final int TOOLS_WINDOWING = 4;
	
    private Cursor stackCursor;
    private Cursor zoomCursor;
    private Cursor handCursor;
    private Cursor contrastCursor;
    
	private Image image;
	
	private ImageBuffer imageBuffer;
	
	private volatile BufferedImage render;
	
	private float brightness;
	
	private float contrast;
	
	private int index;
	private int x, y;
	private double zoom;
	private int selectedTool;
	
	private int windowWidth;
	private int windowHeight;
	
	private boolean showInfo;
	
	private SimpleDateFormat dateFormat;
	
	private boolean zoomFit;
	private boolean center;
	
	public ImageViewer() {
		
		imageBuffer = new ImageBuffer();
		imageBuffer.addImageBufferListener(this);
		
		stackCursor = Toolkit.getDefaultToolkit().createCustomCursor(Toolkit.getDefaultToolkit().createImage(ImageViewer.class.getResource("resources/stack.png")), new Point(), "Scroll");
		zoomCursor = Toolkit.getDefaultToolkit().createCustomCursor(Toolkit.getDefaultToolkit().createImage(ImageViewer.class.getResource("resources/zoom.png")), new Point(), "Zoom");
		handCursor = Toolkit.getDefaultToolkit().createCustomCursor(Toolkit.getDefaultToolkit().createImage(ImageViewer.class.getResource("resources/hand.png")), new Point(), "Move");
		contrastCursor = Toolkit.getDefaultToolkit().createCustomCursor(Toolkit.getDefaultToolkit().createImage(ImageViewer.class.getResource("resources/contrast.png")), new Point(), "Windowing");
		
		showInfo = true;
		
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		enableEvents(AWTEvent.MOUSE_EVENT_MASK
				|AWTEvent.MOUSE_MOTION_EVENT_MASK);
		
		addComponentListener(this);
		addMouseMotionListener(this);
		addMouseWheelListener(this);
		
		initView();
		
		setTool(TOOLS_NONE);

	}
	
	private void initView() {
		
		index = 0;
		x = 0;
		y = 0;
		zoom = 1.0;
		contrast = 1.0f;
		brightness = 10.0f;
		windowWidth = getWidth();
		windowHeight = getHeight();
		
	}
	
	public void paint(Graphics g) {
		
		if(image!=null && render!=null) {
		
			g.drawImage(render,0,0,null);
			g.dispose();
		
		} else {
			
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getWidth(), getHeight());
			g.dispose();
			
		}

	}
	
	private void render() {
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				
				if(imageBuffer.getSize()>0) {
					
					if(center) {
						x = (getWidth() - ((int) Math.round((double) image.getColumns()*zoom)))/2;
						y = (getHeight() - ((int) Math.round((double) image.getRows()*zoom)))/2;
						center = false;
					}
					
					if(zoomFit) {
						double xZoom = (double) getWidth() / (double) image.getColumns();
						double yZoom = (double) getHeight() / (double) image.getColumns();
						zoom = xZoom<yZoom?xZoom:yZoom;
						zoomFit = false;
					}
					
					BufferedImage render = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
					Graphics2D renderg = render.createGraphics();
					renderg.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BICUBIC);
					renderg.setClip(0,0,getWidth(),getHeight());

					renderg.setColor(Color.BLACK);
					renderg.fillRect(0,0,getSize().width,getSize().height);
					renderg.setColor(ImageViewer.this.getForeground());
					if(imageBuffer.getSize()>0) {
						int vx1 = (int) Math.round(x * zoom + getWidth() * (1 - zoom) / 2);
						int vy1 = (int) Math.round(y * zoom + getHeight() * (1- zoom) / 2);
						int vx2 = vx1 + (int) Math.round(image.getColumns() * zoom);
						int vy2 = vy1 + (int) Math.round(image.getRows() * zoom);
						renderg.drawImage(imageBuffer.getBuffer(index),vx1,vy1,vx2,vy2,0,0,image.getColumns(),image.getRows(),ImageViewer.this);
					}
					
					RescaleOp rescale = new RescaleOp(contrast, brightness, null);
					render = rescale.filter(render, null);
					renderg = render.createGraphics();
					
					if(showInfo) {
						
						int row = 0;
			
						Series series = image.getSeries();
						Study study = series.getStudy();
						Patient patient = study.getPatient();
						
						renderg.setColor(Color.DARK_GRAY);
						FontMetrics fontMetrics = getFontMetrics(getFont());
						
						renderg.drawString(patient.getPatientName(), 5, fontMetrics.getHeight() + fontMetrics.getHeight()*row++);
						if(patient.getPatientId()!=null) {
							renderg.drawString(patient.getPatientId(), 5, fontMetrics.getHeight() + fontMetrics.getHeight()*row++);
						}
						if(patient.getPatientBirthDate()!=null) {
							renderg.drawString(dateFormat.format(patient.getPatientBirthDate()), 5, fontMetrics.getHeight() + fontMetrics.getHeight()*row++);
						}
						
						row = 0;
						
						if(series.getInstitutionName()!=null) {
							renderg.drawString(series.getInstitutionName(), getWidth() - fontMetrics.stringWidth(series.getInstitutionName()) - 5, fontMetrics.getHeight() + fontMetrics.getHeight()*row++);
						}
						if(series.getModality()!=null) {
							String text = series.getModality() + (series.getSeriesDescription()!=null?" / "+series.getSeriesDescription():"");
							renderg.drawString(text, getWidth() - fontMetrics.stringWidth(text) - 5, fontMetrics.getHeight() + fontMetrics.getHeight()*row++);
						}
						if(study.getStudyId()!=null) {
							String text = "Study " + study.getStudyId();
							renderg.drawString(text, getWidth() - fontMetrics.stringWidth(text) - 5, fontMetrics.getHeight() + fontMetrics.getHeight()*row++);
						}
						if(series.getSeriesNumber()!=null) {
							String text = "Series " + series.getSeriesNumber();
							renderg.drawString(text, getWidth() - fontMetrics.stringWidth(text) - 5, fontMetrics.getHeight() + fontMetrics.getHeight()*row++);
						}
						if(image.getInstanceNumber()!=null) {
							String text = "Instance " + image.getInstanceNumber();
							renderg.drawString(text, getWidth() - fontMetrics.stringWidth(text) - 5, fontMetrics.getHeight() + fontMetrics.getHeight()*row++);
						}
						String text = "Frame " + (index+1) + "/" + imageBuffer.getSize();
						renderg.drawString(text, getWidth() - fontMetrics.stringWidth(text) - 5, fontMetrics.getHeight() + fontMetrics.getHeight()*row++);
						
					}
					
					renderg.dispose();
					
					ImageViewer.this.render = render;
					
					repaint();
									
				}

			}
		});
	}
	
	public void loadImage(Image image) throws IOException {
		this.image = image;
		imageBuffer.setImage(image);
		initView();
		center = true;
		zoomFit = true;
		render();
	}
	
	public void zoomIn() {
		zoom *= 1.1;
		render();
	}
	
	public void zoomOut() {
		zoom *= 0.9;
		render();
	}
    
	public void addContrast(float value) {
    	contrast += 0.001 * contrast * value + 0.001;
    	if(contrast>256) {
    		contrast = 256f;
    	}
        render();
	}
	
	public void subContrast(float value) {
    	contrast -= 0.001 * contrast * value;
    	if(contrast<0) {
    		contrast = 0.01f;
    	}
        render();
	}

	public void addBrightness(float value) {
		brightness += 0.1 * value;
    	if(brightness>256) {
    		brightness = 256f;
    	}
        render();
	}
	
	public void subBrightness(float value) {
		brightness -= 0.1 * value;
    	if(brightness<-256) {
    		brightness = -256f;
    	}
        render();
	}

    public void setTool(int tool) {
    	
    	selectedTool = tool;
    	
    	switch (selectedTool) {
		case TOOLS_SCROLL:
			setCursor(stackCursor);
			break;
		case TOOLS_ZOOM:
			setCursor(zoomCursor);
			break;
		case TOOLS_MOVE:
			setCursor(handCursor);
			break;
		case TOOLS_WINDOWING:
			setCursor(contrastCursor);
			break;
		default:
			setCursor(Cursor.getDefaultCursor());
			break;
		}
    	
    }

	public void mouseWheelMoved(MouseWheelEvent e) {
		
		if(e.getWheelRotation()<0) {
			previous();
		} else {
			next();
		}
		
	}
	
	private int mouseX = 0;
	private int mouseY = 0;

	private int mouseDX = 0;
	private int mouseDY = 0;

	public void mouseDragged(MouseEvent e) {
		updateMouse(e.getX(), e.getY());
		
    	switch (selectedTool) {
		case TOOLS_SCROLL:
			if(mouseDY>0) {
				previous();
			} else {
				next();
			}
			break;
		case TOOLS_ZOOM:
			if(mouseDY>0) {
				zoomIn();
			} else {
				zoomOut();
			}
			break;
		case TOOLS_MOVE:
			translate(mouseDX,mouseDY);
			break;
		case TOOLS_WINDOWING:
			if(mouseDX>0) {
				subContrast(mouseDX);
			} else {
				addContrast(-mouseDX);
			}
			if(mouseDY>0) {
				addBrightness(mouseDY);
			} else {
				subBrightness(-mouseDY);
			}
			break;
		default:
			break;
		}
    	
	}

	public void mouseMoved(MouseEvent e) {
		updateMouse(e.getX(), e.getY());
	}
	
	private void updateMouse(int x, int y) {
		mouseDX = mouseX - x;
		mouseDY = mouseY - y;
		mouseX = x;
		mouseY = y;
	}
	
	public void translate(int dX, int dY) {
		x -= dX / zoom;
		y -= dY / zoom;
        render();
		repaint();
	}
	
	public boolean hasPrevious() {
		return index>0;
	}
	
	public boolean hasNext() {
		return index<imageBuffer.getSize()-1;
	}
	
	public void previous() {
		if(hasPrevious()) {
			index--;
	        render();
			fireFrameChanged();
		}
	}
	
	public void next() {
		if(hasNext()) {
			index++;
	        render();
			fireFrameChanged();
		}
	}
	
	private Timer cineTimer;
	
	public void play() {
		if(cineTimer==null) {
			cineTimer = new Timer(500, new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(hasNext()) {
						next();
					} else {
						index = 0;
					}
			        render();
					repaint();
				}
			});
			cineTimer.start();
			fireCineModeChanged();
		}
	}
	
	public void stop() {
		if(cineTimer!=null) {
			cineTimer.stop();
			cineTimer = null;
			fireCineModeChanged();
		}
	}

	public boolean isPlaying() {
		return cineTimer!=null;
	}
	
	public void showInfo() {
		showInfo = true;
		render();
	}
	
	public void hideInfo() {
		showInfo = false;
		render();
	}
	
	public boolean isShowingInfo() {
		return showInfo;
	}

	public void componentResized(ComponentEvent e) {
		if(windowWidth>0&&windowHeight>0) {
			double dYZoom = (double) getHeight() / (double) windowHeight;
			zoom *= dYZoom;
			x = x - windowWidth / 2 + getWidth() / 2;
			y = y - windowHeight / 2 + getHeight() / 2;
		}
		windowWidth = getWidth();
		windowHeight = getHeight();
		render();
	}

	public void componentMoved(ComponentEvent e) {
	}

	public void componentShown(ComponentEvent e) {
	}

	public void componentHidden(ComponentEvent e) {
	}

    public void addImageViewerListener(ImageViewerListener l) {
        listenerList.add(ImageViewerListener.class, l);
    }
	
    public void removeImageViewerListener(ImageViewerListener l) {
        listenerList.remove(ImageViewerListener.class, l);
    }
    
    public ImageViewerListener[] getImageViewerListeners() {
        return listenerList.getListeners(ImageViewerListener.class);
    }
    
    public void fireFrameChanged() {
    	ImageViewerListener[] listeners = getImageViewerListeners();
    	ImageViewerEvent event = new ImageViewerEvent(this);
    	for(ImageViewerListener listener : listeners) {
    		listener.frameChanged(event);
    	}
    }

    public void fireCineModeChanged() {
    	ImageViewerListener[] listeners = getImageViewerListeners();
    	ImageViewerEvent event = new ImageViewerEvent(this);
    	for(ImageViewerListener listener : listeners) {
    		listener.cineModeChanged(event);
    	}
    }

	public void imageLoaded(int index) {
		render();
	}

}
