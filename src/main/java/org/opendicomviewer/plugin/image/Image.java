package org.opendicomviewer.plugin.image;

import org.opendicomviewer.model.Instance;

public class Image extends Instance {
	
	private java.awt.Image thumbnail;

	/** Number of Frames (0028,0008) */
	private int numberOfFrames;
	
	/** Rows (0028,0010)  */
	private int rows;
	
	/** Columns (0028,0011) */
	private int columns;

	public java.awt.Image getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(java.awt.Image thumbnail) {
		this.thumbnail = thumbnail;
	}

	public int getNumberOfFrames() {
		return numberOfFrames;
	}

	public void setNumberOfFrames(int numberOfFrames) {
		this.numberOfFrames = numberOfFrames;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

}
