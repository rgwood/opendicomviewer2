package org.opendicomviewer.plugin.image;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.jdesktop.application.Action;

public class ImageController implements ImageViewerListener {
	
	private ImageViewer imageViewer;
	
	private PropertyChangeSupport changeSupport;
	
	private boolean hasPrevious;
	private boolean hasNext;

	public ImageController(ImageViewer imageViewer) {
		this.imageViewer = imageViewer;
		changeSupport = new PropertyChangeSupport(this);
		imageViewer.addImageViewerListener(this);
	}
	
	@Action
	public void image() {
		
	}

	@Action(enabledProperty="hasPrevious")
	public void previousImage() {
		imageViewer.previous();
	}

	@Action(enabledProperty="hasNext")
	public void nextImage() {
		imageViewer.next();
	}

	@Action
	public void playStop() {
		if(!imageViewer.isPlaying()) {
			imageViewer.play();			
		} else {
			imageViewer.stop();
		}
	}

	@Action
	public void selectScroll() {
		imageViewer.setTool(ImageViewer.TOOLS_SCROLL);
	}

	@Action
	public void selectZoom() {
		imageViewer.setTool(ImageViewer.TOOLS_ZOOM);
	}

	@Action
	public void selectMove() {
		imageViewer.setTool(ImageViewer.TOOLS_MOVE);
	}

	@Action
	public void selectContrast() {
		imageViewer.setTool(ImageViewer.TOOLS_WINDOWING);
	}

	@Action
	public void imageInformation() {
		if(!imageViewer.isShowingInfo()) {
			imageViewer.showInfo();			
		} else {
			imageViewer.hideInfo();
		}
	}

	public boolean isHasPrevious() {
		return hasPrevious;
	}

	public void setHasPrevious(boolean hasPrevious) {
		boolean old = this.hasPrevious;
		this.hasPrevious = hasPrevious;
		changeSupport.firePropertyChange("hasPrevious", old, hasPrevious);
	}

	public boolean isHasNext() {
		return hasNext;
	}

	public void setHasNext(boolean hasNext) {
		boolean old = this.hasNext;
		this.hasNext = hasNext;
		changeSupport.firePropertyChange("hasNext", old, hasNext);
	}

	public void frameChanged(ImageViewerEvent imageViewerEvent) {
		setHasPrevious(imageViewer.hasPrevious());
		setHasNext(imageViewer.hasNext());
	}

	public void cineModeChanged(ImageViewerEvent imageViewerEvent) {
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.removePropertyChangeListener(listener);
	}

}
