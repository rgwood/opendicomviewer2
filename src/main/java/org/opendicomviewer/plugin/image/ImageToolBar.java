package org.opendicomviewer.plugin.image;

import javax.swing.ActionMap;
import javax.swing.ButtonGroup;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import org.jdesktop.application.Application;

public class ImageToolBar extends JToolBar implements ImageViewerListener {

	private static final long serialVersionUID = 1L;
	
	private ImageViewer imageViewer;
	private JToggleButton cineModeButton;
	
	public ImageToolBar(ImageController imageController, ImageViewer imageViewer) {
		
		this.imageViewer = imageViewer;
		imageViewer.addImageViewerListener(this);
		
		setOrientation(JToolBar.VERTICAL);
		setFloatable(false);
		
		ActionMap imageActionMap = Application.getInstance().getContext().getActionMap(imageController);

		JToggleButton scrollButton = new JToggleButton(imageActionMap.get("selectScroll"));
		scrollButton.setHideActionText(true);
		add(scrollButton);

		JToggleButton zoomButton = new JToggleButton(imageActionMap.get("selectZoom"));
		zoomButton.setHideActionText(true);
		add(zoomButton);
		
		JToggleButton moveButton = new JToggleButton(imageActionMap.get("selectMove"));
		moveButton.setHideActionText(true);
		add(moveButton);

		JToggleButton windowingButton = new JToggleButton(imageActionMap.get("selectContrast"));
		windowingButton.setHideActionText(true);
		add(windowingButton);

		ButtonGroup toolsGroup = new ButtonGroup();
		toolsGroup.add(scrollButton);
		toolsGroup.add(zoomButton);
		toolsGroup.add(moveButton);
		toolsGroup.add(windowingButton);
		
		addSeparator();
		
		add(imageActionMap.get("previousImage")).setHideActionText(true);
		add(imageActionMap.get("nextImage")).setHideActionText(true);
		
		cineModeButton = new JToggleButton(imageActionMap.get("playStop"));
		cineModeButton.setHideActionText(true);
		add(cineModeButton);

	}

	public void frameChanged(ImageViewerEvent imageViewerEvent) {
	}

	public void cineModeChanged(ImageViewerEvent imageViewerEvent) {
		cineModeButton.setSelected(imageViewer.isPlaying());
	}

}
