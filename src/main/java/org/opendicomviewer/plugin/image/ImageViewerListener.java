package org.opendicomviewer.plugin.image;

import java.util.EventListener;

public interface ImageViewerListener extends EventListener {
	
	public void frameChanged(ImageViewerEvent imageViewerEvent);

	public void cineModeChanged(ImageViewerEvent imageViewerEvent);

}
