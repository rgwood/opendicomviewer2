package org.opendicomviewer.plugin.image;

import java.util.EventObject;

public class ImageViewerEvent extends EventObject {

	private static final long serialVersionUID = 1L;
	
	public ImageViewerEvent(Object source) {
		super(source);
	}

}
