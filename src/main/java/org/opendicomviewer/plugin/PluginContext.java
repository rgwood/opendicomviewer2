/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.plugin;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.opendicomviewer.ApplicationContext;


public class PluginContext {
	
	private ApplicationContext applicationContext;
	
	private HashMap<String,Plugin> pluginMap;
	private HashMap<String,Plugin> pluginIdMap;
	
	public PluginContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
		pluginMap = new HashMap<String, Plugin>();
		pluginIdMap = new HashMap<String, Plugin>();
	}
	
	public void register(String cuid, Plugin plugin) throws PluginException {
		Plugin old = pluginMap.get(cuid);
		if(old!=null) {
			old.uninstall(cuid, applicationContext);
		}
		pluginMap.put(cuid, plugin);
		pluginIdMap.put(plugin.getId(), plugin);
		plugin.install(cuid, applicationContext);
	}

	public void register(String[] cuids, Plugin plugin) throws PluginException {
		for(String cuid : cuids) {
			register(cuid, plugin);
		}
	}
	
	public void unregister(String cuid) throws PluginException {
		Plugin plugin = pluginMap.get(cuid);
		if(plugin!=null) {
			plugin.uninstall(cuid, applicationContext);
			pluginMap.remove(cuid);
		}
	}
	
	public Plugin getPlugin(String cuid) {
		return pluginMap.get(cuid);
	}
	
	public Plugin getPluginById(String id) {
		return pluginIdMap.get(id);
	}
	
	public List<Plugin> getPlugins() {
		return new LinkedList<Plugin>(pluginIdMap.values());
	}

}
