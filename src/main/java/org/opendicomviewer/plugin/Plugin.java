/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.plugin;

import java.awt.Image;

import javax.activation.DataSource;

import org.opendicomviewer.ApplicationContext;
import org.opendicomviewer.model.Instance;

public interface Plugin {
	
	public String getId();
	
	public void install(String cuid, ApplicationContext applicationContext) throws PluginException;

	public void uninstall(String cuid, ApplicationContext applicationContext) throws PluginException;
	
	public Instance createInstance(DataSource dataSource) throws PluginException;
	
	public void viewInstance(Instance instance) throws PluginException;
	
	public Image getThumbnail(Instance instance) throws PluginException;
	
}
