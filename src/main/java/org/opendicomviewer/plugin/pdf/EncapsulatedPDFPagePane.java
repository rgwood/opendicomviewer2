/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.plugin.pdf;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.ImageObserver;

import javax.swing.JPanel;

import com.sun.pdfview.PDFPage;

public class EncapsulatedPDFPagePane extends JPanel implements ComponentListener, ImageObserver {

	private static final long serialVersionUID = 1L;
	
	private Image pageImage;
	
	private PDFPage pdfPage;
	
	private float zoom;
	
	private int curWidth;
	private int curHeight;
	
	public EncapsulatedPDFPagePane() {
		super();
		addComponentListener(this);
		zoom = 1;
	}
	
	@Override
	public void paint(Graphics g) {
		
		if(pageImage==null) {
			synchronized (this) {
				if(pageImage==null) {
					pageImage = pdfPage.getImage(curWidth, curHeight, pdfPage.getBBox(), this);
				}
			}
		}
		
		g.drawImage(pageImage, 0, 0, this);
		
	}
	
	private void calculateSize() {
		curWidth = Math.round(pdfPage.getWidth() * zoom);
		curHeight = Math.round(pdfPage.getHeight() * zoom);
		setPreferredSize(new Dimension(curWidth, curHeight));		
	}

	public void setPdfPage(PDFPage pdfPage) {
		this.pdfPage = pdfPage;
		calculateSize();
		pageImage = null;
		repaint();
	}

	public void setZoom(float zoom) {
		this.zoom = zoom;
		calculateSize();
		repaint();
	}

	@Override
	public boolean imageUpdate(Image img, int infoflags, int x, int y, int w, int h) {
		repaint();
		return super.imageUpdate(img, infoflags, x, y, w, h);
	}
	
	public void componentResized(ComponentEvent e) {
		if(pdfPage!=null) {
			pdfPage.stop(curWidth, curHeight, pdfPage.getBBox());
		}
		pageImage = null;
	}

	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
