/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.plugin.pdf;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.jdesktop.application.Action;

public class EncapsulatedPDFController implements EncapsulatedPDFViewerListener {
	
	private EncapsulatedPDFViewer encapsulatedPDFViewer;
	
	private PropertyChangeSupport changeSupport;
	
	private boolean hasPrevious;
	private boolean hasNext;
	
	public EncapsulatedPDFController(EncapsulatedPDFViewer encapsulatedPDFViewer) {
		this.encapsulatedPDFViewer = encapsulatedPDFViewer;
		changeSupport = new PropertyChangeSupport(this);
		encapsulatedPDFViewer.addEncapsulatedPDFViewerListener(this);
	}
	
	@Action
	public void encapsulatedPDF() {
	}

	@Action(enabledProperty="hasPrevious")
	public void previousPage() {
		encapsulatedPDFViewer.previousPage();
	}

	@Action(enabledProperty="hasNext")
	public void nextPage() {
		encapsulatedPDFViewer.nextPage();
	}

	public boolean isHasPrevious() {
		return hasPrevious;
	}

	public void setHasPrevious(boolean hasPrevious) {
		boolean old = this.hasPrevious;
		this.hasPrevious = hasPrevious;
		changeSupport.firePropertyChange("hasPrevious", old, hasPrevious);
	}

	public boolean isHasNext() {
		return hasNext;
	}

	public void setHasNext(boolean hasNext) {
		boolean old = this.hasNext;
		this.hasNext = hasNext;
		changeSupport.firePropertyChange("hasNext", old, hasNext);
	}

	public void pageChanged(EncapsulatedPDFViewerEvent encapsulatedPDFViewerEvent) {
		setHasPrevious(encapsulatedPDFViewer.hasPrevious());
		setHasNext(encapsulatedPDFViewer.hasNext());
	}
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.removePropertyChangeListener(listener);
	}

}
