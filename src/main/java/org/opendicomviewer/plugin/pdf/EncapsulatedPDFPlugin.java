/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.plugin.pdf;

import java.awt.BorderLayout;
import java.awt.Image;
import java.io.IOException;

import javax.activation.DataSource;
import javax.imageio.ImageIO;
import javax.swing.ActionMap;
import javax.swing.JMenu;
import javax.swing.JPanel;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.StopTagInputHandler;
import org.jdesktop.application.Application;
import org.opendicomviewer.ApplicationContext;
import org.opendicomviewer.model.Instance;
import org.opendicomviewer.plugin.Plugin;
import org.opendicomviewer.plugin.PluginException;

public class EncapsulatedPDFPlugin implements Plugin {
	
	private EncapsulatedPDFViewer encapsulatedPDFViewer;
	private EncapsulatedPDFController encapsulatedPDFController;
	private EncapsulatedPDFToolBar encapsulatedPDFToolBar;
	private JPanel view;
	private JMenu menu;
	
	private boolean installed;
	
	public EncapsulatedPDFPlugin() {
		
		encapsulatedPDFViewer = new EncapsulatedPDFViewer();
		encapsulatedPDFController = new EncapsulatedPDFController(encapsulatedPDFViewer);
		encapsulatedPDFToolBar = new EncapsulatedPDFToolBar(encapsulatedPDFController);
		
		view = new JPanel();
		view.setLayout(new BorderLayout());
		view.add(encapsulatedPDFToolBar,BorderLayout.WEST);
		view.add(encapsulatedPDFViewer,BorderLayout.CENTER);
		
		menu = createMenu();
		
	}

	public void install(String cuid, ApplicationContext applicationContext) {
		
		if(!installed) {
			applicationContext.getViewContext().getViewerManager().register(getId(),view);
			applicationContext.getViewContext().getMenuManager().register(getId(), menu);
			installed = true;
		}
		
	}

	public void uninstall(String cuid, ApplicationContext applicationContext) {
		
		if(installed) {
			applicationContext.getViewContext().getViewerManager().unregister(getId());
			applicationContext.getViewContext().getMenuManager().unregister(getId());
			installed = false;
		}
		
	}

	public void viewInstance(Instance instance) throws PluginException {
		try {
			encapsulatedPDFViewer.loadEncapsulatedPDF((EncapsulatedPDF) instance);
		} catch (IOException e) {
			e.printStackTrace();
			throw new PluginException(e.getMessage(),e);
		}
	}

	public Image getThumbnail(Instance instance) throws PluginException {
		try {
			return ImageIO.read(EncapsulatedPDFPlugin.class.getResource("resources/pdf.png"));
		} catch (IOException e) {
			e.printStackTrace();
			throw new PluginException(e.getMessage(),e);
		}
	}

	public String getId() {
		return "ENCAPSULATED PDF PLUGIN";
	}

	private JMenu createMenu() {
		
		ActionMap actionMap = Application.getInstance().getContext().getActionMap(encapsulatedPDFController);
		
		menu = new JMenu(actionMap.get("encapsulatedPDF"));
		menu.setVisible(false);
		menu.add(actionMap.get("previousPage"));
		menu.add(actionMap.get("nextPage"));

		return menu;

	}

	public Instance createInstance(DataSource dataSource) throws PluginException {
		
		try {
		
			DicomObject dcmObj = null;
			DicomInputStream din = new DicomInputStream(dataSource.getInputStream());
			din.setHandler(new StopTagInputHandler(Tag.PixelData - 1));
			dcmObj = new BasicDicomObject();
			din.readDicomObject(dcmObj,-1);
			din.close();
	
			EncapsulatedPDF encapsulatedPDF = new EncapsulatedPDF();
			encapsulatedPDF.setDataSource(dataSource);
			encapsulatedPDF.setSopClassUID(dcmObj.getString(Tag.SOPClassUID));
			encapsulatedPDF.setSopInstanceUID(dcmObj.getString(Tag.SOPInstanceUID));
			encapsulatedPDF.setAcquisitionDateTime(dcmObj.getDate(Tag.AcquisitionDateTime));
			encapsulatedPDF.setInstanceNumber(dcmObj.getString(Tag.InstanceNumber));
			encapsulatedPDF.setTitle(dcmObj.getString(Tag.DocumentTitle));
			return encapsulatedPDF;
			
		} catch (IOException e) {
			e.printStackTrace();
			throw new PluginException(e.getMessage(), e);
		}
		
	}

}
