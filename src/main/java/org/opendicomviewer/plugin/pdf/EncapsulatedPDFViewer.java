/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.plugin.pdf;

import java.awt.BorderLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.activation.DataSource;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.util.CloseUtils;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

public class EncapsulatedPDFViewer extends JPanel implements ComponentListener {

	private static final long serialVersionUID = 1L;
	
	private PDFPage currentPage;
	private int currentPageNumber;
	private EncapsulatedPDFPagePane pagePane;
	
	private PDFFile pdfFile;
	
	private JScrollPane scrollPane;

	public EncapsulatedPDFViewer() {
		super();
		addComponentListener(this);
		setLayout(new BorderLayout());
		pagePane = new EncapsulatedPDFPagePane();
		scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.getVerticalScrollBar().setUnitIncrement(60);
		add(scrollPane,BorderLayout.CENTER);
	}
	
	public void loadEncapsulatedPDF(EncapsulatedPDF encapsulatedPDF) throws IOException {
		
		ByteBuffer buf = ByteBuffer.wrap(extractPDFFile(encapsulatedPDF.getDataSource()));
		pdfFile = new PDFFile(buf);
		goToPage(1);

	}
	
	private byte[] extractPDFFile(DataSource dataSource) throws IOException {
		
		byte[] bytes = null;
		DicomInputStream din = null;
		
		try {
			
		    din = new DicomInputStream(dataSource.getInputStream());
		    DicomObject dcmObj = din.readDicomObject();
		    
		    bytes = dcmObj.getBytes(Tag.EncapsulatedDocument);
		    
		}
		finally {
		    CloseUtils.safeClose(din);
		}
		
		return bytes;
		
	}
	
	private void goToPage(int pageNumber) {
		if(pageNumber<1) {
			pageNumber = 1;
		}
		if(pageNumber>pdfFile.getNumPages()) {
			pageNumber = pdfFile.getNumPages();
		}
		currentPage = pdfFile.getPage(pageNumber);
		pagePane.setPdfPage(currentPage);
		currentPageNumber = pageNumber;
		fitZoom();
		scrollPane.setViewportView(pagePane);
		firePageChanged();
	}
	
	public void previousPage() {
		if (hasPrevious()) goToPage(currentPageNumber - 1);
	}

	public void nextPage() {
		if (hasNext()) goToPage(currentPageNumber + 1);
	}
	
	public boolean hasPrevious() {
		return currentPageNumber>1;
	}
	
	public boolean hasNext() {
		return currentPageNumber<pdfFile.getNumPages();
	}
	
	private void fitZoom() {
		pagePane.setZoom(scrollPane.getViewport().getWidth()/currentPage.getWidth());
	}

	public void componentResized(ComponentEvent e) {
		if(currentPage!=null) {
			fitZoom();
		}
	}

	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}
	
    public void addEncapsulatedPDFViewerListener(EncapsulatedPDFViewerListener l) {
        listenerList.add(EncapsulatedPDFViewerListener.class, l);
    }
	
    public void removeEncapsulatedPDFViewerListener(EncapsulatedPDFViewerListener l) {
        listenerList.remove(EncapsulatedPDFViewerListener.class, l);
    }
    
    public EncapsulatedPDFViewerListener[] getEncapsulatedPDFViewerListeners() {
        return listenerList.getListeners(EncapsulatedPDFViewerListener.class);
    }
    
    public void firePageChanged() {
    	EncapsulatedPDFViewerListener[] listeners = getEncapsulatedPDFViewerListeners();
    	EncapsulatedPDFViewerEvent event = new EncapsulatedPDFViewerEvent(this);
    	for(EncapsulatedPDFViewerListener listener : listeners) {
    		listener.pageChanged(event);
    	}
    }

}
