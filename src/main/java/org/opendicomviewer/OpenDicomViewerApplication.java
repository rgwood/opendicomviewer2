/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.net.URI;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.jdesktop.application.Application;
import org.opendicomviewer.view.OpenDicomViewerFrame;

public class OpenDicomViewerApplication extends Application {

	private static final long serialVersionUID = 1L;
	
	private OpenDicomViewer openDicomViewer;
	
	public OpenDicomViewerApplication() {
		openDicomViewer = new OpenDicomViewer();
	}
	
	public void startup() {
		
		try {
	        UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
	    } 
	    catch (UnsupportedLookAndFeelException e) {
	    	e.printStackTrace();
	    }
	    catch (ClassNotFoundException e) {
	    	e.printStackTrace();
	    }
	    catch (InstantiationException e) {
	    	e.printStackTrace();
	    }
	    catch (IllegalAccessException e) {
	    	e.printStackTrace();
	    }

	    try {
	    	ApplicationContext applicationContext = openDicomViewer.getApplicationContext();
	    	applicationContext.getViewContext().getMenuManager().addMenuManagerListener(applicationContext.getViewContext().getOpenDicomViewerFrame());
	    	openDicomViewer.startup();
	    	OpenDicomViewerFrame fr=applicationContext.getViewContext().getOpenDicomViewerFrame();
	    	Dimension dim=Toolkit.getDefaultToolkit().getScreenSize();
	    	fr.setSize(dim.width>>1, dim.height>>1);
	    	fr.setLocation(1, 1);
	    	fr.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void shutdown() {
		
		try {
			openDicomViewer.shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
    public OpenDicomViewer getOpenDicomViewer() {
		return openDicomViewer;
	}

	private static final String USAGE =
        "java -jar OpenDicomViewer.jar [-dicomdir <DICOMDIR>] | [<dcmurl> ...]";
    private static final String DESCRIPTION = 
        "OpenDicomViewer.jar options:";
    private static final String EXAMPLE = null;

	public static void main(String[] args) {
		
		final CommandLine cl = parse(args);
		
		if(cl!=null && !cl.hasOption('h')) {
			Application.launch(OpenDicomViewerApplication.class, args);
			SwingUtilities.invokeLater(new Runnable() {
				@SuppressWarnings("unchecked")
				public void run() {
					
					try {

						ApplicationContext applicationContext = Application.getInstance(OpenDicomViewerApplication.class).getOpenDicomViewer().getApplicationContext();

						if(cl.hasOption("dicomdir")) {
							File dicomDir = new File(cl.getOptionValue("dicomdir"));
							if(dicomDir.exists()) {
								applicationContext.getControllerContext().getFileController().openDicomDir(dicomDir);
							}
						} else if(!cl.getArgList().isEmpty()) {
							List<String> argList = (List<String>) cl.getArgList();
							URI[] uris = new URI[argList.size()];
							for(int i=0;i<argList.size();i++) {
								String arg = argList.get(i);
								uris[i] = new URI(arg);
							}
							applicationContext.getControllerContext().getFileController().openDicomURIs(uris);
						} else {
							File dicomDir = new File("DICOMDIR");
							if(dicomDir.exists()) {
								applicationContext.getControllerContext().getFileController().openDicomDir(dicomDir);
							}
						}
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
						Application.getInstance().exit();
					}
									
				}
			});
		
		}
		
	}
	
	private static CommandLine parse(String[] args) {
		
		Options opts = new Options();
		opts.addOption(new Option("dicomdir", true, "Open a DICOMDIR."));
		opts.addOption("h", "help", false, "print this message");
        CommandLine cl = null;
        try {
            cl = new PosixParser().parse(opts, args);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            System.err.println("Try 'java -jar OpenDicomViewer.jar -h' for more information.");
        }
        if (cl.hasOption('h')) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(USAGE, DESCRIPTION, opts, EXAMPLE);
        }
        return cl;		
		
	}

}
