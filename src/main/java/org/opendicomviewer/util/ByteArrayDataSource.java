/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.DataSource;

public class ByteArrayDataSource implements DataSource {

	private final String contentType;
	private final byte[] byteArray;
	 
	public ByteArrayDataSource(byte[] byteArray) {
		this(byteArray, "application/octet-stream");
	}

	public ByteArrayDataSource(byte[] byteArray, String contentType) {
		this.byteArray = byteArray;
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}

	public InputStream getInputStream() {
		return new ByteArrayInputStream(byteArray);
	}

	public String getName() {
		return null;
	}

	public OutputStream getOutputStream() throws IOException {
		throw new UnsupportedOperationException();
	}
}
