/* ***** BEGIN LICENSE BLOCK *****
 * This file is part of OpenDicomViewer.
 * 
 * OpenDicomViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * OpenDicomViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with OpenDicomViewer.  If not, see <http://www.gnu.org/licenses/>
 * 
 * Original source code was developed and released by Sytia Informática SL,
 * Huelva/Spain.
 *
 * Contributor(s):
 * Abdul Wahab Sultán Regalado <awsultan@gmail.com>
 * Javier Cámara Pérez <jotacamara@gmail.com>
 * 
 * ***** END LICENSE BLOCK ***** */

package org.opendicomviewer.model;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Patient {
	
	/** The patient id (0010,0020). */
	private String patientId;
	
	/** The patient name in DICOM format (0010,0010). */
	private String patientName;
	
	/** The patient's birth date (0010,0030). */
	private Date patientBirthDate;
	
	/** The patient's gender (0010,0040). */
	private String patientSex;
	
	private LinkedList<Study> studies;
	
	public Patient() {
		studies = new LinkedList<Study>();
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Date getPatientBirthDate() {
		return patientBirthDate;
	}

	public void setPatientBirthDate(Date patientBirthDate) {
		this.patientBirthDate = patientBirthDate;
	}

	public String getPatientSex() {
		return patientSex;
	}

	public void setPatientSex(String patientSex) {
		this.patientSex = patientSex;
	}

	public List<Study> getStudies() {
		return Collections.unmodifiableList(studies);
	}

	public void addStudy(Study study) {
		studies.addLast(study);
	}
	
}
